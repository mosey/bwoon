var audio = new Audio();
var bwoon = $('#bwoon');

if(audio.canPlayType('audio/ogg')){
    audio.src = "assets/sounds/inception-sound.ogg";
} else if(audio.canPlayType('audio/mpeg')){
    audio.src = "assets/sounds/inception-sound.mp3";
} else if(audio.canPlayType('audio/wav')){
    audio.src = "assets/sounds/inception-sound.wav";
}


audio.addEventListener('loadedmetadata', function() {
    $('#bwoon, h1').addClass('visible');
    var mildur = (audio.duration) * 1000;
    bwoon.bind('click', function() {
        if(!audio.paused) {
           return 
        } else {
            audio.play();
            $(this).addClass('shake shake-soft shake-constant');  
            $('body').addClass('angry');  
            setTimeout(function(){
                if(bwoon.hasClass('shake shake-soft shake-constant')){
                    $('#bwoon, body').removeAttr('class');
                    bwoon.addClass('button visible');
                }
            }, mildur);
        }
    });
});